using NUnit.Framework;

namespace Entities.Tests
{
    public class SkyTests
    {
        [Test]
        public void CanBeCreated()
        {
            // When
            var sky = new Sky();
            
            // Then
            Assert.NotNull(sky);
        }
    }
}