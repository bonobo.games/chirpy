using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Entities.Tests
{
    public class MapTests
    {
        [Test]
        public void CanBeCreated()
        {
            // When
            var map = new Map();

            // Then
            Assert.NotNull(map);
        }
        
        [Test]
        public void ShouldConsistOfGivenGroundAndSky()
        {
            // Given
            var ground = new Ground();
            var sky = new Sky();
            
            // When
            var map = new Map(ground, sky);
            
            // Then
            Assert.AreSame(ground, map.Ground);
            Assert.AreSame(sky, map.Sky);
        }

        [Test]
        public void ShouldBeRollingInADirection()
        {
            // Given
            var map = new Map();
            var vanishingPoints = new List<Tuple<float, float>>();
            
            // When
            foreach (var _ in Enumerable.Range(0, 100))
            {
                map.Advance();
                vanishingPoints.Add(new Tuple<float, float>(map.MinDistance, map.MaxDistance));
            }
            
            // Then
            Tuple<float, float> Diff(Tuple<float, float> p0, Tuple<float, float> p1) =>
                new(p1.Item1 - p0.Item1, p1.Item2 - p0.Item2);

            for (var i = 1; i < vanishingPoints.Count; i++)
            {
                var diff = Diff(vanishingPoints[i - 1], vanishingPoints[i]);
                Assert.Less(0.0f, diff.Item1);
                Assert.Less(0.0f, diff.Item2);
            }
        }
    }
}
