using NUnit.Framework;

namespace Entities.Tests
{
    public class GroundTests
    {
        [Test]
        public void CanBeCreated()
        {
            // When
            var ground = new Ground();
            
            // Then
            Assert.NotNull(ground);
        }
    }
}