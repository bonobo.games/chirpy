using NUnit.Framework;

namespace Entities.Tests
{
    public class ChirpyTests
    {
        [Test]
        public void CanBeCreated()
        {
            // When
            var chirpy = new Chirpy();

            // Then
            Assert.NotNull(chirpy);
        }
    }
}
