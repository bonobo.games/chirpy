﻿namespace Entities
{
    public class Map
    {
        public Map(Ground ground = null, Sky sky = null)
        {
            Ground = ground;
            Sky = sky;
        }

        public Ground Ground { get; }
        public Sky Sky { get; }
        public float MinDistance { get; private set; } = 0.0f;
        public float MaxDistance { get; private set; } = 1.0f;

        public void Advance()
        {
            MinDistance += 1.0f;
            MaxDistance += 1.0f;
        }
    }
}
