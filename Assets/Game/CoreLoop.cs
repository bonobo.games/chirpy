﻿using Entities;

namespace Game
{
    public class CoreLoop
    {
        public CoreLoop(Map map, Chirpy chirpy)
        {
            Map = map;
            Chirpy = chirpy;
        }

        public Map Map { get; }
        public Chirpy Chirpy { get; }
    }
}
