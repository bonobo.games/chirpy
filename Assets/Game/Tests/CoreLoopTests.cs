using Entities;
using NUnit.Framework;

namespace Game.Tests
{
    public class CoreLoopTests
    {
        [Test]
        public void ShouldConsistOfGivenMapAndChirpy()
        {
            // Given
            var map = new Map();
            var chirpy = new Chirpy();
            
            // When
            var coreLoop = new CoreLoop(map, chirpy);
            
            // Then
            Assert.AreSame(map, coreLoop.Map);
            Assert.AreSame(chirpy, coreLoop.Chirpy);
        }
    }
}
