# Chirpy
### _(a game that we should have made with my brother, while we could)_

Chirpy is a simple bird, living in a simple world. He loves nature and all the things that the universe can offer.
He likes to fly and walk, just for fun, not because it's his job or duty, but because he's born a bird, and that's what birds do.
Being free and happy and exploring the beauty of the world is everything to him.
He never stops, until he finally does. And then all starts over...

## Features

- You can escort Chirpy through his infinite journey
- You can trigger him flap his wings to start flying or to raise higher
- He would always glide lower and lower until he'd reach the ground again, then he'd walk
- Chirpy stops by for anything he can't fly through, until you decide to help him get around things
- Happiness is granted when he gets in contact with nature or when he can just fly or walk freely
- The world never stops, only Chirpy does, if he faces any obstacle. Then we'd rather help him out
- The more time he accumulates blocked, while life is going on, the closer he gets to the end of joy
- His life is about joy and freedom. Without that he can't exist
